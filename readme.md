### Trabalho de Programação para Internet 2

#### Requisitos

---

- **NodeJS** [(v16.14.2)](https://nodejs.org/en/)
- **Postgresql** - Conexão de banco de dados
- **Variáveis de ambiente** (Por questões de seguraça, as configurações de conexão do banco devem ser configuradas como variáveis de ambiente)
  - **DB_USER** (Usuário do banco de dados)
  - **DB_PASSWORD** (Senha do banco de dados)
  - **DB_DATABASE** (Nome da base do banco de dados)
  - **DB_HOST** (Endereço ou IP do banco de dados)
  - **DB_PORT** (Porta de conexão do banco de dados)

## Rodando o projeto

> Caso não queria rodar o código, o site foi disponibilizado na seguinte URL: http://trabalho-pi-2.herokuapp.com

Após instalar o NodeJS, ir até o diretório do projeto e rodar os comandos abaixo em ordem:

#### Estrutura do banco

Na raiz do projeto existe uma pasta chamada "arquivos". Nela tem um arquivo **.sql** com os comandos necessários para criar o banco

Diagrama do banco de dados
![Diagrama do banco](/arquivos/Diagrama.jpeg?raw=true "Diagrama do banco")

### Antes de rodar o projeto!!!

Antes de rodar o projeto é necessário configurar as variáveis de ambiente.
Para isso, existe um arquivo chamado ".exempla.env". É necessário renomea-lo para ".env".
Dentro deste arquivo já tera as variáveis necessárias, apenas é preciso colocar os valores corretos.

#### Utilizando o NPM

`npm install`

`npm run dev`

> Deverá aparecer a seguinte mensagem: "Server started on port: XXXX"

#### Utilizando o YARN

`yarn`

`yarn dev`

> Deverá aparecer a seguinte mensagem: "Server started on port: XXXX"

#### Após iniciar o servidor

Após a mensagem informando que o servidor foi iniciar, é necessário acessar a seguinte URL: http://localhost:PORT_INFORMADA_NA_MENSAGEM

### Videos

Como rodar o projeto: https://www.youtube.com/watch?v=q9SZQpCrlWk

Apresentação do projeto: https://www.youtube.com/watch?v=L1vrLdSMQ2o

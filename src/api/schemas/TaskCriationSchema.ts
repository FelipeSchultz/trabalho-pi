import Joi from 'joi'
import { SchemaErrorMessages } from '../config/SchemaErrorMessages'
import { ITaskCreationBody } from '../shared/interfaces/TaskCreationBody'


export const taskCreationSchema = Joi.object<ITaskCreationBody>({
    listId: Joi.number().required(),
    taskName: Joi.string().trim().required(),
    description: Joi.string().allow('', null)
}).options({
    abortEarly: false,
    messages: SchemaErrorMessages
})
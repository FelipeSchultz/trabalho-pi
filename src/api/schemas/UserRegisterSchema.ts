import Joi from 'joi'
import { IUserRegisterBody } from '../shared/interfaces/UserRegisterBody'
import { SchemaErrorMessages } from '../config/SchemaErrorMessages'


export const userRegisterSchema = Joi.object<IUserRegisterBody>({
    name: Joi.string().trim().required(),
    password: Joi.string().alphanum().min(6).required(),
    email: Joi.string().email().required()
}).options({
    abortEarly: false,
    messages: SchemaErrorMessages
})
import Joi from 'joi'
import { SchemaErrorMessages } from '../config/SchemaErrorMessages'
import { ITaskFlagUpdateBody } from '../shared/interfaces/TaskFlagUpdateBody'


export const taskFlagUpdateSchema = Joi.object<ITaskFlagUpdateBody>({
    taskId: Joi.number().required(),
    flagStatus: Joi.boolean()
}).options({
    abortEarly: false,
    messages: SchemaErrorMessages
})
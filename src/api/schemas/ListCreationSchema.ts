import Joi from 'joi'
import { SchemaErrorMessages } from '../config/SchemaErrorMessages'
import { IListCreationBody } from '../shared/interfaces/ListCreationBody'


export const listCreationSchema = Joi.object<IListCreationBody>({
    name: Joi.string().trim().required(),
    color: Joi.string().trim().required()
}).options({
    abortEarly: false,
    messages: SchemaErrorMessages
})
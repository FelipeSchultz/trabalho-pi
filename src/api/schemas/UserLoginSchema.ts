import Joi from 'joi'
import { SchemaErrorMessages } from '../config/SchemaErrorMessages'
import { IUserLoginBody } from '../shared/interfaces/UserLoginBody'


export const userLoginSchema = Joi.object<IUserLoginBody>({
    password: Joi.string().alphanum().min(6).required(),
    email: Joi.string().email().required()
}).options({
    abortEarly: false,
    messages: SchemaErrorMessages
})
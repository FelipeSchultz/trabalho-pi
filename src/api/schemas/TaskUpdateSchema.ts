import Joi from 'joi'
import { SchemaErrorMessages } from '../config/SchemaErrorMessages'
import { ITaskUpdateBody } from '../shared/interfaces/TaskUpdateBody'


export const taskUpdateSchema = Joi.object<ITaskUpdateBody>({
    taskName: Joi.string().trim().required(),
    description: Joi.string().allow('', null)
}).options({
    abortEarly: false,
    messages: SchemaErrorMessages
})
export interface IUserTokenVerifyQuery {
    token: string;
}
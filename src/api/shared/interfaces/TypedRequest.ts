import { Request } from 'express'
import { ParamsDictionary } from 'express-serve-static-core';

export type TypedBodyRequest<BodyType = {}> = Request<ParamsDictionary, {}, BodyType>;
export type TypedQueryRequest<QueryType = {}> = Request<ParamsDictionary, {}, {}, QueryType>;
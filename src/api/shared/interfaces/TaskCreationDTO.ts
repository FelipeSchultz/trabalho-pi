export interface ITaskCreationDTO {
    listId: number;
    taskName: string;
    description: string
}
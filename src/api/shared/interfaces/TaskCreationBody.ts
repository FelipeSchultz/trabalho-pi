export interface ITaskCreationBody {
    listId: number;
    taskName: string;
    description: string
}
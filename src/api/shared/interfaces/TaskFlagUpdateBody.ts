export interface ITaskFlagUpdateBody {
    taskId: number;
    flagStatus: boolean;
}
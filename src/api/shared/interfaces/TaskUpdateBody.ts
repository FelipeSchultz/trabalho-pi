export interface ITaskUpdateBody {
    taskName: string;
    description: string
}
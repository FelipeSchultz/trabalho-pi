export interface IUserLoginBody {
    password: string;
    email: string
}
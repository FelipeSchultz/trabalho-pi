export interface IUserLoginResponse {
    user: {
        name: string;
        email: string;
    },
    token: string;
}
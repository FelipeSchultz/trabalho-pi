import { IUserLoginBody } from "./UserLoginBody";

export interface IUserRegisterBody extends IUserLoginBody {
    name: string;
}
export interface IListCreationDTO {
    userId: number;
    name: string;
    color: string
}
export const SchemaErrorMessages: Record<string, string> = {
    'any.required': '#.is.required',
    'string.empty': '#.cant.be.empty'
}
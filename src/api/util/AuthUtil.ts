import { sign, SignOptions, verify, decode } from 'jsonwebtoken'

const { SECRET, TOKEN_EXPIRATION } = process.env

const signOptions: SignOptions = {
    expiresIn: TOKEN_EXPIRATION
}

export const generateToken = (payload: object | string | Buffer) => {
    return sign(payload, SECRET, signOptions)
}

export const verifyToken = (token: string) => {
    verify(token, SECRET)
}

export const decodeToken = (token: string) => {
    return decode(token) as DecodedToken
}

interface DecodedToken {
    id_usuario: number,
    nom_usuario: string,
    des_email: string,
    iat: number,
    exp: number
}
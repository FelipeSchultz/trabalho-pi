import { ValidationError } from 'joi'

export const validatorErrorMessageResponsify = (error: ValidationError) => error.details.map(e => {
    const { context: { key }, message } = e;

    return { field: key, message: message.replace('#', key) }
})
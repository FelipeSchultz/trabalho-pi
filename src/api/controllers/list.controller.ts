
import { Request, Response } from 'express';
import listRepository from '../repositories/list.repository';
import taskRepository from '../repositories/task.repository';
import { IListCreationBody } from '../shared/interfaces/ListCreationBody';
import { IListCreationDTO } from '../shared/interfaces/ListCreationDTO';
import { TypedBodyRequest } from '../shared/interfaces/TypedRequest';
import { decodeToken } from '../util/AuthUtil';

class ListController {
    async create(req: TypedBodyRequest<IListCreationBody>, res: Response) {
        const { authorization } = req.headers
        const { name, color } = req.body

        const { id_usuario } = decodeToken(authorization);

        const listDTO: IListCreationDTO = {
            userId: id_usuario,
            name,
            color
        }

        listRepository.createList(listDTO)
            .then(() => {
                return res.status(201).send();
            })
            .catch((err) => {
                return res.status(500).json({ message: 'An error occurred on server', err })
            });
    }

    async getLists(req: Request, res: Response) {
        const { authorization } = req.headers

        const { id_usuario } = decodeToken(authorization);

        listRepository.getListByUserId(id_usuario)
            .then((select) => {
                const responseDTO = select.rows.map(row => ({
                    listId: row.id_lista,
                    listName: row.nom_lista,
                    color: row.cor_lista,
                    createdAt: row.dta_criacao
                }))

                return res.status(200).json({ result: responseDTO });
            })
            .catch((err) => {
                return res.status(500).json({ message: 'An error occurred on server', err })
            });
    }

    async getListById(req: Request, res: Response) {
        const { listId } = req.params
        const { id_usuario } = decodeToken(req.headers.authorization)

        const listResult = await listRepository.getListById(id_usuario, Number(listId));

        if (listResult.rowCount !== 1) {
            return res.status(500).json({ message: 'An error occurred on server' })
        }

        try {
            const { id_lista, nom_lista, dta_criacao, cor_lista } = listResult.rows[0]

            const notCompletedTasks = await listRepository.getNotCompletedTasks(id_lista);
            const completedTasks = await listRepository.getCompletedTasks(id_lista);

            const mapFunction = (row) => {
                const { id_tarefa, nom_tarefa, flg_concluido, obs_tarefa } = row;

                return {
                    taskId: id_tarefa,
                    taskName: nom_tarefa,
                    description: obs_tarefa,
                    completed: flg_concluido
                }
            }

            const mappedNotCompleted = notCompletedTasks.rows.map(mapFunction)
            const mappedCompleted = completedTasks.rows.map(mapFunction)

            const responseBody = {
                listId: id_lista,
                listName: nom_lista,
                listColor: cor_lista,
                createdAt: dta_criacao,
                notCompleted: mappedNotCompleted,
                completed: mappedCompleted,
            }

            return res.status(200).json({ result: responseBody });
        } catch (error) {
            return res.status(500).json({ message: 'An error occurred on server', error })
        }
    }

    async delete(req: Request, res: Response) {
        const { listId } = req.params;
        const { id_usuario } = decodeToken(req.headers.authorization)

        const { rowCount } = await listRepository.getListById(id_usuario, Number(listId))

        if (rowCount < 1) {
            return res.status(500).json({ message: 'An error occurred on server' })
        }

        try {
            await taskRepository.deleteAllById(listId)
            await listRepository.deleteById(listId)

            return res.status(200).send();
        } catch (error) {
            return res.status(500).json({ message: 'An error occurred on server' })
        }
    }

    async update(req: TypedBodyRequest<IListCreationBody>, res: Response) {
        const { name, color } = req.body
        const { listId } = req.params
        const { id_usuario } = decodeToken(req.headers.authorization)

        const { rows, rowCount } = await listRepository.getListById(id_usuario, Number(listId))

        if (rowCount < 1) {
            return res.status(500).json({ message: 'An error occurred on server' })
        }

        const { nom_lista, cor_lista } = rows[0]

        const newName = name || nom_lista
        const newColor = color || cor_lista

        listRepository.update(id_usuario, listId, newName, newColor)
            .then(() => res.status(200).send())
            .catch(err => res.status(500).json({ message: 'An error occurred on server', err }))
    }
}

const listController = new ListController();

export { listController };
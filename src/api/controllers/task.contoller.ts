import { Request, Response } from 'express';
import listRepository from '../repositories/list.repository';
import taskRepository from '../repositories/task.repository';
import { ITaskCreationBody } from '../shared/interfaces/TaskCreationBody';
import { ITaskCreationDTO } from '../shared/interfaces/TaskCreationDTO';
import { ITaskFlagUpdateBody } from '../shared/interfaces/TaskFlagUpdateBody';
import { TypedBodyRequest } from '../shared/interfaces/TypedRequest';
import { decodeToken } from '../util/AuthUtil';

class TaskController {
    async create(req: TypedBodyRequest<ITaskCreationBody>, res: Response) {
        const { authorization } = req.headers
        const { listId, taskName, description } = req.body

        const { id_usuario } = decodeToken(authorization);

        const { rowCount } = await listRepository.getListById(id_usuario, listId);

        if (rowCount < 1) {
            return res.status(500).json({ message: 'An error occurred on server' })
        }

        const listDTO: ITaskCreationDTO = {
            listId,
            taskName,
            description
        }

        taskRepository.createTask(listDTO)
            .then(() => {
                return res.status(201).send();
            })
            .catch((err) => {
                return res.status(500).json({ message: 'An error occurred on server', err })
            });
    }

    async updateFlagStatus(req: TypedBodyRequest<ITaskFlagUpdateBody>, res: Response) {
        const { taskId, flagStatus } = req.body;

        taskRepository.updateFlagStatus(taskId, flagStatus)
            .then(() => res.status(204).send())
            .catch(err => res.status(500).json({ message: 'An error occurred on server', err }))
    }

    async deleteById(req: Request, res: Response) {
        const { taskId } = req.params;
        const { id_usuario } = decodeToken(req.headers.authorization);

        const { rowCount } = await taskRepository.getTaskById(id_usuario, taskId)

        if (rowCount < 1) {
            return res.status(500).json({ message: 'An error occurred on server' })
        }

        taskRepository.deleteById(taskId)
            .then(() => res.status(204).send())
            .catch(err => res.status(500).json({ message: 'An error occurred on server', err }))
    }

    async update(req: TypedBodyRequest<ITaskCreationBody>, res: Response) {
        const { taskName, description } = req.body
        const { taskId } = req.params
        const { id_usuario } = decodeToken(req.headers.authorization)

        const { rowCount, rows } = await taskRepository.getTaskById(id_usuario, taskId)

        if (rowCount < 1) {
            return res.status(500).json({ message: 'An error occurred on server' })
        }

        const { nom_tarefa, obs_tarefa } = rows[0]

        const newName = taskName || nom_tarefa
        const newDescription = description || obs_tarefa

        taskRepository.updateByID(taskId, newName, newDescription)
            .then(() => res.status(200).send())
            .catch(err => res.status(500).json({ message: 'An error occurred on server', err }))
    }

    async getById(req: Request, res: Response) {
        const { taskId } = req.params
        const { id_usuario } = decodeToken(req.headers.authorization)

        try {
            const { rowCount, rows } = await taskRepository.getTaskById(id_usuario, taskId)

            if (rowCount < 1) {
                return res.status(500).json({ message: 'An error occurred on server' })
            }

            const { nom_tarefa, obs_tarefa } = rows[0]

            const responseBody = {
                taskName: nom_tarefa,
                description: obs_tarefa
            }

            return res.status(200).json({ result: responseBody })
        } catch (error) {
            return res.status(500).json({ message: 'An error occurred on server', error })
        }
    }
}

const taskController = new TaskController();

export { taskController };
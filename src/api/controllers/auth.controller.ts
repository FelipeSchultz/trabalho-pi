
import { Response } from 'express';
import { JsonWebTokenError } from 'jsonwebtoken';
import { TypedBodyRequest, TypedQueryRequest } from '../shared/interfaces/TypedRequest';
import { IUserLoginBody } from '../shared/interfaces/UserLoginBody';
import { IUserLoginResponse } from '../shared/interfaces/UserLoginResponse';
import { IUserRegisterBody } from '../shared/interfaces/UserRegisterBody';
import { IUserTokenVerifyQuery } from '../shared/interfaces/UserTokenVerifyQuery';
import { generateToken, verifyToken } from '../util/AuthUtil';
import UserRepository from './../repositories/user.repository'

class AuthController {
    async register(req: TypedBodyRequest<IUserRegisterBody>, res: Response) {
        UserRepository.createUser(req.body)
            .then(() => {
                return res.status(201).send();
            })
            .catch(() => {
                return res.status(500).json({ message: 'An error occurred on server' })
            });
    }

    async login(req: TypedBodyRequest<IUserLoginBody>, res: Response) {
        try {
            const userInfo = await UserRepository.getUserByLogin(req.body);

            if (!userInfo) {
                return res.status(500).json({ message: 'Incorret login' })
            }

            const userResponse: IUserLoginResponse = {
                user: {
                    name: userInfo.nom_usuario,
                    email: userInfo.des_email
                },
                token: generateToken(userInfo)
            }

            return res.status(200).json(userResponse)
        } catch {
            return res.status(500).json({ message: 'An error occurred on server' })
        }
    }

    async verify(req: TypedQueryRequest<IUserTokenVerifyQuery>, res: Response) {
        try {
            const { token } = req.query;

            if (!token) {
                throw new Error("Token missing!");
            }

            verifyToken(token)

            return res.status(200).json({ valid: true })
        } catch (error) {
            if (error instanceof JsonWebTokenError) {
                const { message } = error

                return res.status(200).json({ valid: false, message })
            }

            return res.status(500).json({ message: 'An error occurred on server' })
        }
    }
}

const authController = new AuthController();

export { authController };
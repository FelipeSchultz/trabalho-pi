import express from 'express'
import { listController } from '../controllers/list.controller';
import { validator } from '../middlewares/validation/Validator';
import { listCreationSchema } from '../schemas/ListCreationSchema';

const listRouter = express.Router()

listRouter.put('/:listId', validator(listCreationSchema), listController.update);
listRouter.post('', validator(listCreationSchema), listController.create);
listRouter.get('', listController.getLists);
listRouter.get('/:listId', listController.getListById);
listRouter.delete('/:listId', listController.delete);

export { listRouter };
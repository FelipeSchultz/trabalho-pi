//taskController
import express from 'express'
import { taskController } from '../controllers/task.contoller';
import { validator } from '../middlewares/validation/Validator';
import { taskCreationSchema } from '../schemas/TaskCriationSchema';
import { taskFlagUpdateSchema } from '../schemas/TaskFlagUpdateSchema';
import { taskUpdateSchema } from '../schemas/TaskUpdateSchema';

const taskRouter = express.Router()

taskRouter.put('/:taskId', validator(taskUpdateSchema), taskController.update);
taskRouter.post('', validator(taskCreationSchema), taskController.create);
taskRouter.put('', validator(taskFlagUpdateSchema), taskController.updateFlagStatus);
taskRouter.delete('/:taskId', taskController.deleteById);
taskRouter.get('/:taskId', taskController.getById);

export { taskRouter };
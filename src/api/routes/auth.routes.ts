import express from 'express'
import { authController } from '../controllers/auth.controller';
import { validator } from '../middlewares/validation/Validator';
import { userLoginSchema } from '../schemas/UserLoginSchema';
import { userRegisterSchema } from '../schemas/UserRegisterSchema';

const authRouter = express.Router()

authRouter.post('/register', validator(userRegisterSchema), authController.register);
authRouter.post('/login', validator(userLoginSchema), authController.login);
authRouter.get('/verify', authController.verify);

export { authRouter };
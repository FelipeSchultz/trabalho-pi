import { Client, ClientConfig } from 'pg'

const { DB_USER, DB_PASSWORD, DB_DATABASE, DB_HOST, DB_PORT } = process.env

const config: ClientConfig = {
    user: DB_USER,
    password: DB_PASSWORD,
    database: DB_DATABASE,
    host: DB_HOST,
    port: Number(DB_PORT)
}

const dbClient = new Client(config);

dbClient.connect();

export { dbClient };
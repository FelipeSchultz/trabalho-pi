import { dbClient } from "../db/db.connection";
import { ITaskCreationDTO } from "../shared/interfaces/TaskCreationDTO";
import { QUERIES } from './data/sql'

class TaskRepository {
    async createTask(taskCreation: ITaskCreationDTO) {
        const { listId, taskName, description } = taskCreation

        return dbClient.query(QUERIES.CREATE_TASK, [listId, taskName, description])
    }

    async updateFlagStatus(taskId: number, status: boolean) {
        return dbClient.query(QUERIES.UPDATE_FLG_STATUS_TASK, [status, taskId])
    }

    async deleteAllById(listId: string | number) {
        return dbClient.query(QUERIES.DELETE_ALL_TASK_BY_ID, [listId])
    }

    async getTaskById(userId, taskId) {
        return dbClient.query(QUERIES.CHECK_TASK_BY_USER_ID, [userId, taskId])
    }

    async deleteById(taskId) {
        return dbClient.query(QUERIES.DELETE_TASK_BY_ID, [taskId])
    }

    async updateByID(taskId, name, description) {
        return dbClient.query(QUERIES.UPDATE_TASK, [taskId, name, description])
    }
}

export default new TaskRepository();
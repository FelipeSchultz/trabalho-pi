import { dbClient } from "../db/db.connection";
import { IListCreationDTO } from "../shared/interfaces/ListCreationDTO";
import { QUERIES } from './data/sql'

class ListRepository {
    async createList(listCreation: IListCreationDTO) {
        const { userId, name, color } = listCreation

        return dbClient.query(QUERIES.CREATE_LIST, [userId, name, color])
    }

    async getListByUserId(userId: number) {
        return dbClient.query(QUERIES.SELECT_LISTS_BY_USER_ID, [userId])
    }

    async getListById(userId: number, listId: number) {
        return dbClient.query(QUERIES.SELECT_LIST_BY_ID, [userId, listId])
    }

    async getNotCompletedTasks(listId: number) {
        return dbClient.query(QUERIES.SELECT_NOT_COMPLETED_TASKS, [listId])
    }

    async getCompletedTasks(listId: number) {
        return dbClient.query(QUERIES.SELECT_COMPLETED_TASKS, [listId])
    }

    async deleteById(listId) {
        return dbClient.query(QUERIES.DELETE_LIST_BY_ID, [listId])
    }

    async update(userId, listId, name, color) {
        return dbClient.query(QUERIES.UPDATE_LIST, [userId, listId, name, color])
    }
}

export default new ListRepository();
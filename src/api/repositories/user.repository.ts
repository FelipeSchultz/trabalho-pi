import { dbClient } from "../db/db.connection";
import { IUserLoginBody } from "../shared/interfaces/UserLoginBody";
import { IUserRegisterBody } from "../shared/interfaces/UserRegisterBody";
import { QUERIES } from './data/sql'
import { IFindUserByLogin } from "./interfaces/FindUserByLogin";

class UserRepository {
    async getAll() {
        return dbClient.query(QUERIES.SELECT_USERS);
    }

    async createUser(userRegister: IUserRegisterBody) {
        const { name, email, password } = userRegister

        return dbClient.query(QUERIES.CREATE_USER, [name, email, password])
    }

    async getUserByLogin(userLogin: IUserLoginBody): Promise<IFindUserByLogin> {
        const { email, password } = userLogin

        const { rowCount, rows } = await dbClient.query(QUERIES.SELECT_USER_BY_LOGIN, [email, password]);

        if (rowCount > 1) {
            throw new Error("more than one row selected");
        }

        return rows[0]
    }
}

export default new UserRepository();
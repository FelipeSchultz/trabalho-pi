import { NextFunction, Response } from 'express'
import { ObjectSchema } from 'joi'
import { TypedBodyRequest } from '../../shared/interfaces/TypedRequest'
import { validatorErrorMessageResponsify } from '../../util/ValidationUtil'

export const validator = <Type = {}>(schema: ObjectSchema<Type>) => {
    const callback = (req: TypedBodyRequest<Type>, res: Response, next: NextFunction) => {
        const { error } = schema.validate(req.body);

        if (error) {
            return res.status(400).json({ message: validatorErrorMessageResponsify(error) })
        }

        next();
    }

    return callback;
}
import { NextFunction, Request, Response } from "express";
import { JsonWebTokenError } from "jsonwebtoken";
import { verifyToken } from "../util/AuthUtil";

export const authenticationMiddleware = (req: Request, res: Response, next: NextFunction) => {
    try {
        const { authorization } = req.headers;

        if (!authorization) {
            throw new Error("Token missing!");
        }

        verifyToken(authorization)

        next()
    } catch (error) {
        if (error instanceof JsonWebTokenError) {
            const { message } = error

            return res.status(200).json({ valid: false, message })
        }

        return res.status(500).json({ message: 'An error occurred on server', error })
    }
}
import { baseURL, http } from '../../services/AxiosService.js'

const getLoginFormBody = () => {
    const { elements: { email, password } } = document.forms.namedItem('login')

    return {
        email: email.value,
        password: password.value
    }
}

const login = async () => {
    try {
        const { data } = await http.post('/api/auth/login', getLoginFormBody())

        window.sessionStorage.setItem('session', JSON.stringify(data))
        window.location.href = `${baseURL}/site/pages/home/home.html`
    } catch {
        console.error('authentication failed!')
    }
}

const setLoginFormSubmitEvent = () => {
    const form = document.forms.namedItem('login')

    form.addEventListener('submit', e => {
        e.preventDefault()
        login()
    })
}

window.onload = () => {
    setLoginFormSubmitEvent()
};
import { http, baseURL } from '../../services/AxiosService.js'

const checkAuthentication = async () => {
    const session = JSON.parse(window.sessionStorage.getItem('session'))

    if (!session) {
        redirectToLogin()
    }

    try {
        const { data } = await http.get(`/api/auth/verify?token=${session.token}`)

        if (!data.valid) {
            redirectToLogin()
        }
    } catch (error) {
        redirectToLogin()
    }
}
const redirectToLogin = () => {
    window.location.href = baseURL
}
const createList = async () => {
    const name = document.getElementById('modal-name');
    const color = document.getElementById('modal-color');

    const body = {
        name: name.value,
        color: color.value
    }

    const session = JSON.parse(window.sessionStorage.getItem('session'))

    try {
        await http.post('/api/list', body, {
            headers: {
                authorization: session.token
            }
        })

        const modal = document.getElementById('modal');
        document.getElementById('modal-name').value = '';

        modal.classList.add('hiden')

        getLists()

    } catch (error) {
        console.error(`Error: ${error}`)
    }
}
const getLists = async () => {
    const session = JSON.parse(window.sessionStorage.getItem('session'))

    try {
        const { data } = await http.get('/api/list', {
            headers: {
                authorization: session.token
            }
        })

        buildViewList(data.result)

        if (data.result.length > 0) {
            const { listId } = data.result[0]

            return listId;
        }
    } catch (error) {
        console.error(`Error: ${error}`)
    }
}
const buildViewList = (list = []) => {
    const section = document.getElementById('list-rows')

    section.textContent = ''

    if (!list || !list.length) {
        const span = document.createElement('div')

        span.innerHTML = 'Sem projetos'

        return section.appendChild(btn)
    }


    list.forEach(l => {
        const dot = document.createElement('div')
        const span = document.createElement('span')
        const btn = document.createElement('button')

        dot.classList.add('dot')
        dot.style.backgroundColor = l.color

        span.innerText = l.listName

        btn.classList.add('list')

        btn.value = l.listId


        btn.appendChild(dot)
        btn.appendChild(span)

        btn.addEventListener('click', (event) => showSelectedList(event.target.value))

        section.appendChild(btn)
    })
}
const showSelectedList = async (value) => {
    const listId = Number(value);

    window.localStorage.setItem('selectedListId', listId)

    try {
        const session = JSON.parse(window.sessionStorage.getItem('session'))

        const { data } = await http.get(`/api/list/${listId}`, {
            headers: {
                authorization: session.token
            }
        })

        buildShowSelectList(data.result)
    } catch (error) {
        console.error(error)
    }
}
const buildShowSelectList = (result) => {
    if (result) {
        document.getElementById('container').classList.remove('hiden')
    }
    const { listId, listName, createdAt, notCompleted, completed } = result;

    const date = new Date(createdAt)

    const title = document.getElementById('title-wrapper')

    const hasDeleteButton = document.getElementById('delete-btn')
    const hasEditButton = document.getElementById('edit-btn')

    if (!hasDeleteButton) {
        const deleteBtn = createDeleteButton('delete-btn')
        deleteBtn.value = listId
        deleteBtn.addEventListener('click', deleteList)

        title.appendChild(deleteBtn)
    } else {
        hasDeleteButton.value = listId
    }

    if (!hasEditButton) {
        const editBtn = createEditButton('edit-btn')
        editBtn.value = listId
        editBtn.addEventListener('click', openModalEditList)

        title.appendChild(editBtn)
    } else {
        hasEditButton.value = listId
    }

    document.getElementById('list-container-header-title').innerText = listName
    document.getElementById('list-container-header-date').innerText = ((date.getDate())) + "/" + ((date.getMonth() + 1)) + "/" + date.getFullYear();

    const notCompletedContainer = document.getElementById('not-completed')
    const completedContainer = document.getElementById('completed')

    notCompletedContainer.innerText = ''
    completedContainer.innerText = ''

    const buildFunction = (container) => {
        return task => {
            const { taskId, taskName, description, completed } = task;

            const desc = description ? `<span class="description">${description}</span>` : ''

            const label = textToElement(`
                <label class="task" name="task-${taskId}">
                    <div class="task-wrapper">
                        <input id="status-input-${taskId}" name="status" type="checkbox" ${completed ? 'checked' : ''} value="${taskId}">
                        <div class="dot"></div>
                        <div class="info">
                            <span>${taskName}</span>
                            ${desc}
                        </div>
                    </div>
                </label>
            `)

            container.appendChild(label)

            const buttonsContainer = textToElement('<div class="buttons-container"></div>')

            const deleteButton = createDeleteButton('', taskId)
            deleteButton.addEventListener('click', deleteTask)

            const editButton = createEditButton('', taskId)
            editButton.addEventListener('click', openEditTask)

            document.getElementById(`status-input-${taskId}`).addEventListener('change', updateTaskStatus)
            buttonsContainer.appendChild(deleteButton)
            buttonsContainer.appendChild(editButton)

            label.appendChild(buttonsContainer)
        }
    }

    notCompleted.forEach(buildFunction(notCompletedContainer))
    completed.forEach(buildFunction(completedContainer))
}
function textToElement(text) {
    const template = document.createElement('template');
    text = text.trim();
    template.innerHTML = text;
    return template.content.firstChild;
}

const createDeleteButton = (idName, value) => {
    return textToElement(`
        <button id="${idName}" value="${value}" class="action-button">
            <svg width="24" height="24">
                <g fill="none" fill-rule="evenodd">
                    <path d="M0 0h24v24H0z"></path>
                    <rect width="14" height="1" x="5" y="6" fill="currentColor" rx=".5"></rect>
                    <path fill="currentColor" d="M10 9h1v8h-1V9zm3 0h1v8h-1V9z"></path>
                    <path stroke="currentColor" d="M17.5 6.5h-11V18A1.5 1.5 0 0 0 8 19.5h8a1.5 1.5 0 0 0 1.5-1.5V6.5zm-9 0h7V5A1.5 1.5 0 0 0 14 3.5h-4A1.5 1.5 0 0 0 8.5 5v1.5z"></path>
                </g>
            </svg>
        </button>
    `)
}

const createEditButton = (idName, value) => {
    return textToElement(`
        <button id="${idName}" value="${value}" class="action-button">
            <svg width="24" height="24">
                <g fill="none" fill-rule="evenodd">
                    path fill="currentColor" d="M9.5 19h10a.5.5 0 110 1h-10a.5.5 0 110-1z"></path>
                    <path stroke="currentColor" d="M4.42 16.03a1.5 1.5 0 00-.43.9l-.22 2.02a.5.5 0 00.55.55l2.02-.21a1.5 1.5 0 00.9-.44L18.7 7.4a1.5 1.5 0 000-2.12l-.7-.7a1.5 1.5 0 00-2.13 0L4.42 16.02z"></path>
                </g>
            </svg>
        </button>
    `)
}

const createEditTask = (taskId) => {
    return textToElement(`
        <div id="task-edit-container" class="task-container">
            <fieldset>
                <input type="text" id="edit-task-name" placeholder="ex: Pagar a conta de luz" onkeyup="checkDisableEditTaskButton()">

                <textarea id="edit-task-description" placeholder="Descrição"></textarea>
            </fieldset>

            <footer>
                <button class="primary-btn" value="${taskId}" id="save-edit-btn">Salvar</button>
                <button class="secondary-btn" value="${taskId}" id="cancel-edit-btn">Canelar</button>
            </footer>
        </div>
    `)
}
const createTask = async () => {
    const name = document.getElementById('task-name')
    const description = document.getElementById('task-description')

    const listId = localStorage.getItem('selectedListId')

    const body = {
        listId: listId,
        taskName: name.value,
        description: description.value
    }

    const session = JSON.parse(window.sessionStorage.getItem('session'))

    try {
        await http.post('/api/task', body, {
            headers: {
                authorization: session.token
            }
        })

        showSelectedList(listId).then(() => {
            name.value = ''
            description.value = ''
        })

    } catch (error) {
        console.error(`Error: ${error}`)
    }
}
const updateTaskStatus = async (event) => {
    console.log(1);
    const { value, checked } = event.target;
    const listId = localStorage.getItem('selectedListId')

    const body = {
        taskId: value,
        flagStatus: checked
    }

    const session = JSON.parse(window.sessionStorage.getItem('session'))

    try {
        await http.put('/api/task', body, {
            headers: {
                authorization: session.token
            }
        })

        showSelectedList(listId)

    } catch (error) {
        console.error(`Error: ${error}`)
    }
}

const deleteList = async (event) => {
    const { value } = event.target;

    try {
        const session = JSON.parse(window.sessionStorage.getItem('session'))

        await http.delete(`/api/list/${value}`, {
            headers: {
                authorization: session.token
            }
        })

        document.getElementById('container').classList.add('hiden')
        getLists()
    } catch (error) {
        console.error(error)
    }
}

const openModalEditList = async (event) => {
    const { value } = event.target

    const modal = document.getElementById('edit-list-modal');
    modal.classList.remove('hiden')

    try {
        const session = JSON.parse(window.sessionStorage.getItem('session'))

        const { data } = await http.get(`/api/list/${value}`, {
            headers: {
                authorization: session.token
            }
        })

        const { listName, listColor } = data.result

        console.log(listName, listColor);

        document.getElementById('edit-modal-name').value = listName
        document.getElementById('edit-modal-color').value = listColor
        document.getElementById('save-button').value = value
    } catch (error) {
        console.error(error);
    }
}

const setButtonsEvents = () => {
    document.getElementById('add-button').addEventListener('click', createList)
    document.getElementById('task-add-btn').addEventListener('click', createTask)
    document.getElementById('save-button').addEventListener('click', updateList)
}

const deleteTask = async (event) => {
    const { value } = event.target;

    try {
        const session = JSON.parse(window.sessionStorage.getItem('session'))

        const listId = localStorage.getItem('selectedListId')

        await http.delete(`/api/task/${value}`, {
            headers: {
                authorization: session.token
            }
        })

        showSelectedList(listId)
    } catch (error) {
        console.error(error)
    }
}

const openEditTask = async (event) => {
    const { value } = event.target;

    if (document.getElementById('task-edit-container')) {
        return
    }

    let label = document.getElementById('not-completed').children.namedItem(`task-${value}`)
        || document.getElementById('completed').children.namedItem(`task-${value}`)

    if (label) {
        label.parentElement.insertBefore(createEditTask(value), label.nextElementSibling)
    }

    document.getElementById('save-edit-btn').addEventListener('click', updateTask)
    document.getElementById('cancel-edit-btn').addEventListener('click', closeTaskEdit)

    try {
        const session = JSON.parse(window.sessionStorage.getItem('session'))

        const { data } = await http.get(`/api/task/${value}`, {
            headers: {
                authorization: session.token
            }
        })

        const { taskName, description } = data.result

        document.getElementById('edit-task-name').value = taskName
        document.getElementById('edit-task-description').value = description

    } catch (error) {
        console.error(error)
    }

}

const closeTaskEdit = () => {
    const container = document.getElementById('task-edit-container')
    container.parentElement.removeChild(container)
}

const updateList = async (event) => {
    const { value } = event.target

    const listName = document.getElementById('edit-modal-name').value
    const listColor = document.getElementById('edit-modal-color').value

    try {
        const session = JSON.parse(window.sessionStorage.getItem('session'))

        const body = {
            name: listName,
            color: listColor
        }

        await http.put(`/api/list/${value}`, body, {
            headers: {
                authorization: session.token
            }
        })

        const listId = localStorage.getItem('selectedListId')


        getLists()
        showSelectedList(listId)

        document.getElementById('edit-list-modal').classList.add('hiden')

    } catch (error) {
        console.error(error);
    }
}

const updateTask = async (event) => {
    const { value } = event.target

    const taskName = document.getElementById('edit-task-name').value
    const description = document.getElementById('edit-task-description').value

    const body = { taskName, description }

    try {
        const session = JSON.parse(window.sessionStorage.getItem('session'))

        await http.put(`/api/task/${value}`, body, {
            headers: {
                authorization: session.token
            }
        })

        const listId = localStorage.getItem('selectedListId')
        showSelectedList(listId)

        document.getElementById('edit-list-modal').classList.add('hiden')

    } catch (error) {
        console.error(error);
    }
}

const setWelcomeName = () => {
    const { user } = JSON.parse(window.sessionStorage.getItem('session'));

    const welcome = document.getElementById('welcome')

    welcome.innerText = `Olá, ${user.name}!`
    welcome.title = user.email
}

window.onload = () => {
    checkAuthentication()
    setButtonsEvents()
    getLists()
        .then(id => {
            if (id) {
                showSelectedList(id)
            }
        })

    setWelcomeName()
};
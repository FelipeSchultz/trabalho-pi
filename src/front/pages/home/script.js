function toogleSideviewMenu() {
    const element = document.getElementById('sideview');

    element.classList.contains('open') ? element.classList.remove('open') : element.classList.add('open')
}

function changeListStatus(e) {
    const element = document.getElementById('lists-wrapper');

    e.checked ? element.classList.add('open') : element.classList.remove('open')
}

function openModal() {
    const modal = document.getElementById('modal');

    modal.classList.remove('hiden')
}

function closeModal() {
    const modal = document.getElementById('modal');

    modal.classList.add('hiden')
}

function checkDisableStatusModalButton() {
    const name = document.getElementById('modal-name');
    const button = document.getElementById('add-button')

    button.disabled = !name.value
}

function checkDisableStatusAddtaskButton() {
    const name = document.getElementById('task-name');
    const button = document.getElementById('task-add-btn')

    button.disabled = !name.value
}
function showTaskOrHideAdd() {
    const btn = document.getElementById('add-task')
    const container = document.getElementById('task-add-container')

    container.classList.contains('hiden') ? btn.classList.add('hiden') : btn.classList.remove('hiden')
    btn.classList.contains('hiden') ? container.classList.remove('hiden') : container.classList.add('hiden')
}

function closeEditModal() {
    const modal = document.getElementById('edit-list-modal');

    modal.classList.add('hiden')
}

function checkDisableEditModalButton() {
    const name = document.getElementById('edit-modal-name');
    const button = document.getElementById('save-button')

    button.disabled = !name.value
}

function checkDisableEditTaskButton() {
    const name = document.getElementById('edit-task-name');
    const button = document.getElementById('save-edit-btn')

    button.disabled = !name.value
}

function logout() {
    window.sessionStorage.clear()
    window.localStorage.clear()

    window.location.href = window.location.origin;
}
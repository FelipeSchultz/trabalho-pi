import { baseURL, http } from '../../services/AxiosService.js'

const getSigninFormBody = () => {
    const { elements: { name, email, password } } = document.forms.namedItem('signin')

    return {
        name: name.value,
        email: email.value,
        password: password.value
    }
}

const signin = async () => {
    const { status } = await http.post('/api/auth/register', getSigninFormBody())

    if (status === 201) {
        window.location.href = baseURL
    }
}

const setSigninFormSubmitEvent = () => {
    const form = document.forms.namedItem('signin')

    form.addEventListener('submit', e => {
        e.preventDefault()
        signin()
    })
}

window.onload = () => {
    setSigninFormSubmitEvent()
};
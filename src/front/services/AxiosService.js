const baseURL = window.location.origin

const http = axios.create({
    baseURL
})

export { http, baseURL };
import 'dotenv/config'

import express from 'express'
import { authRouter } from './api/routes/auth.routes';
import { listRouter } from './api/routes/list.routes';
import { authenticationMiddleware } from './api/middlewares/Authentication';
import { taskRouter } from './api/routes/task.routes';

const server = express();

server.use(express.json())

server.use('/api/auth', authRouter)
server.use('/api/list', authenticationMiddleware, listRouter)
server.use('/api/task', authenticationMiddleware, taskRouter)
server.use('/site', express.static('src/front'))

server.get('', (_req, res) => {
    res.redirect('/site/pages/login/login.html')
})

const { SERVER_PORT } = process.env;

server.listen(SERVER_PORT, () => {
    console.log(`Server started on port: ${SERVER_PORT}`);
})
